/*

	Node.js Introduction

	check node version on terminal using :
		node --version


	-use the "require" directive to load Node.js modules
	-MODULE is a software component or part of a program that contains one or more routines 
	-the HTTP MODULE lets Node.js transfer data using the Hyper Text Transfer Protocol
	- the HTTP MODULE is a set of individual files that contain code to create a COMPONENT that helps establish data transfer between applications
	-HTTP is a protocol that allows the fetching of resources such as HTML documents
	-clients or browser and servers communicate by exchanging individual messages
	-the messages sent by the client, usually a web browser usuall called requests
	-the messages sent by the server as an answer  are called responses
*/

let http = require("http");

// using this module's createServer() method, we can create an HTTP server that listens to requests on a specific port and gives responses back to the client

// the http module has a createServer() method that accepts a function as an argument and allows for a creation of a server.

// the arguments passed in the function are request and response objects (data type) that contains method that allows us to receive requests from the client and send responses back to it

http.createServer(function(request, response){

	/*
	use the writerHead() method to :
		-set a status code for the response. 200 means OK
		-set the content-type of the response as a plain text message
	*/
	response.writeHead(200, {"Content-Type": "text/plain"});

	// send the response with a text content "Hello World"
	response.end("Hello World!");

	/*
		A port is a virtual point where network connections start and end

		each port is associated with a specific process or service

		the server will be assigned to port 4000
		 via the "listen(4000)" method whwere the server will listen to any requests that are sent to it, eventuall communicating with our server
	*/
	
}).listen(4000);

console.log("Server running at localhost: 4000");

